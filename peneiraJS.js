/**
 * PeneiraJs v0.1.0
 * 
 * PeneiraJS higieniza e valida dados de um objeto. A intenção é facilitar
 * a validação de formulários HTML bem como a higienização das informações antes
 * que elas sejam processadas no backend.
 * 
 * Copyright (C) 2018 Joabe Braga.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *   
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
var PeneiraJS = (function() {
    var version = '0.1.0',

    /**
     * Objeto de dados do schema a ser validado.
     * 
     * @type object
     */
    dataObject = {},

    /**
     * Armazena os dados processados/higienizados pelos métodos de filtros. Este
     * objeto será retornado na função "run";
     * 
     * @type object
     */
    filteredDataObject = {},

    /**
     * Objeto principal contendo um ou mais esquemas fitlros e regras. Os 
     * esquemas são indexados pelos ID's definidos dentro do próprio esquema.
     * 
     * @type object
     */
    schema = {},

    /**
     * Objeto de restrições (definições de filtros e regras).
     * 
     * @type object
     */
    schemaProperties = {},

    /**
     * Lista de campos obrigatórios.
     * 
     * @type array
     */
    schemaRequiredFields = [],

    /**
     * Armazena as mensagens de erro produzidas durante o processamento das
     * regras.
     * 
     * @type array
     */
    arrMensagens = [],

    filter = {
        /**
         * Remove os espaços em branco do início e do final de uma string.
         * 
         * @param string value 
         */
        trim: function(value) {
            if (typeof value !== 'string') {
                return '';
            }
            
            return value.trim();
        },

        /**
         * Remove de uma string todos os caracteres (incluindo espaços) que não 
         * sejam dígitos (0-9).
         * 
         * @param string value 
         */
        sanitizeDigits: function(value) {
            if (typeof value !== 'string') {
                return '';
            }
            
            return value.replace(/[^0-9]/g,'');
        },

        /**
         * Converte uma data em um array no seguinte formato: array('DD', 'MM', 'YYYY').
         * 
         * @param string date data em um dos seguintes formatos: DD/MM/YYYY ou YYYY-MM-DD
         * @return array|boolean
         */
        dateToArray: function(date) {
            var stringSplit = '';

            // Checa o formato da data.
            if (rule.checkDateFormatBR(date, null)) {
                stringSplit = '/';
            } else if (rule.checkDateFormatISO(date, null)) {
                stringSplit = '-';
            } else {
                return false;
            }

            var arrDate = date.split(stringSplit);
            
            if (stringSplit === '-') {
                // Inverte as povições do array.
                arrDate = arrDate.reverse();
            }

            return arrDate;
        },

        /**
         * Converte o formato de uma data ISO (YYYY-MM-DD) para o formato 
         * brasileiro (DD/MM/YYYY) ou vice-versa.
         * 
         * @param string date Data no formato "DD/MM/YYY" ou "YYYY-MM-DD"
         * @return string
         */
        dateReverseFormat: function(date) {
            var stringSplit = ''
                newSeparator = '';

            // Checa o formato da data.
            if (rule.checkDateFormatBR(date, null)) {
                stringSplit = '/';
                newSeparator = '-';
            } else if (rule.checkDateFormatISO(date, null)) {
                stringSplit = '-';
                newSeparator = '/';
            } else {
                return date;
            }

            var arrDate = date.split(stringSplit);
            return arrDate[2] + newSeparator + arrDate[1] + newSeparator + arrDate[0];
        }
    },

    rule = {
        /**
         * Verifica se um campo existe e se não é vazio.
         * 
         * @param string fieldName nome do campo
         */
        required: function(fieldName) {
            if (!dataObject.hasOwnProperty(fieldName)) {
                return false;
            }

            var value = dataObject[fieldName];

            if (typeof dataObject[fieldName] === 'string') {
                value = dataObject[fieldName].trim();
            }

            return (value !== false && !value) ? false : true;
        },

        /**
         * Verifica se o tipo de dado de "value" está de acordo com o tipo
         * definido no argumento "type".
         * 
         * Esta função suporta quatro dos seis tipos de dados primitivos do
         * padrão ECMAScript, são eles: boolean, null, number e string. Além 
         * desses foi adicionado o tipo array.
         * 
         * @param boolean|null|number|string|array Valor a ser testado.
         * @param string                           Tipo de dado.
         * 
         * @return boolean 
         */
        type: function(value, type) {
            switch(type) {
                case 'boolean':
                    return (typeof value === 'boolean');
                case 'null':
                    return (value === null);
                case 'number':
                    return /^-?\d+(\.\d+)?$/.test(value);
                case 'string':
                    return (typeof value === 'string');
                case 'array':
                    return (Array.isArray(value));
                default:
                    throw 'PeneiraJS não suporta o tipo dado ' + type + '.';
            }
        },

        /**
         * @param string value e-mail a ser validado
         * @param null   param 
         */
        email: function(value, param) {
            var filtro = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            return filtro.test(value);
        },

        /**
         * Valida um número de CPF.
         * 
         * Modo de usar: { "cpf": true }
         * 
         * @param string value número do CPF (sem máscara) a ser validado.
         * @param null   param 
         */
        cpf: function(value, param) {
            if (!value || typeof value !== 'string') {
                return false;
            }

            var Soma = 0, Resto;

            if (value == "00000000000") {
                return false;
            }
            
            for (var i = 1; i <= 9; i++) {
                Soma = Soma + parseInt(value.substring(i-1, i)) * (11 - i);
            }

            Resto = (Soma * 10) % 11;
            
            if ((Resto == 10) || (Resto == 11)) {
                Resto = 0;
            }

            if (Resto != parseInt(value.substring(9, 10))) {
                return false;
            }
            
            Soma = 0;

            for (var i = 1; i <= 10; i++) {
                Soma = Soma + parseInt(value.substring(i-1, i)) * (12 - i);
            }

            Resto = (Soma * 10) % 11;
            
            if ((Resto == 10) || (Resto == 11)) {
                Resto = 0;
            }

            if (Resto != parseInt(value.substring(10, 11) )) {
                return false;
            }

            return true;
        },

        /**
         * discuss at: http://phpjs.org/functions/checkdate/
         * original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
         * improved by: Pyerre
         * improved by: Theriault
         *  example 1: checkdate(12, 31, 2000);
         *  returns 1: true
         *  example 2: checkdate(2, 29, 2001);
         *  returns 2: false
         *  example 3: checkdate(3, 31, 2008);
         *  returns 3: true
         *  example 4: checkdate(1, 390, 2000);
         *  returns 4: false
         * 
         * Esta função recebeu uma pequena adaptação para se adequar melhor ao 
         * formato brasileiro de data.
         *
         * Modo de usar: { "date": true }
         * 
         * @param string date Aceita os seguintes formatos: 'DD/MM/YYYY',
         *                    'YYYY-MM-DD' ou vazio.
         * 
         * @return boolean
         */
        date: function(date) {
            if (!date || (typeof date === 'string' && !date.trim())) {
                return true;
            }

            var stringSplit = '';

            // Checa o formato da data.
            if (rule.checkDateFormatBR(date, null)) {
                stringSplit = '/';
            } else if (rule.checkDateFormatISO(date, null)) {
                stringSplit = '-';
            } else {
                return false;
            }

            var arrDate = date.split(stringSplit);
            
            if (stringSplit === '-') {
                // Inverte as povições do array.
                arrDate = arrDate.reverse();
            }

            var d = parseInt(arrDate[0]),
                m = parseInt(arrDate[1]),
                y = parseInt(arrDate[2]);
            
            return m > 0 && m < 13 && y > 0 && y < 32768 && d > 0 && d <= (new Date(y, m, 0)).getDate();
        },

        /**
         * Verifica se um valor está contido na lista.
         * 
         * Modo de usar: { "enum": ['value', 'value', 'value'] }
         * 
         * @param string|number value
         * @param array         list
         * 
         * @return boolean
         */
        "enum": function(value, list) {
            if (!Array.isArray(list) || !list.hasOwnProperty(0)) {
                throw 'O argumento de enum deve ser um array não vazio.';
            }

            for (var i = list.length -1; i >= 0; i--) {
                if (value == list[i]) {
                    return true;
                }
            }

            return false;
        },

        /**
         * Verifica se uma string está dentro do seu limite de tamanho.
         * 
         * Modo de usar: { "maxLength": 255 }
         * 
         * @param string  value
         * @param integer length Inteiro não negativo.
         * 
         * @return boolean
         */
        maxLength: function(value, length) {
            if (typeof value !== 'string' || false === rule.type(length, 'number') || length < 0) {
                throw 'Um ou mais argumentos de maxLength são inválidos.';
            }

            return (value.length > length) ? false : true;
        },

        /**
         * Verifica se o tamanho de uma string é igual ou maior ao valor mínimo 
         * definido.
         * 
         * Modo de usar: { "minLength": 1 }
         * 
         * @param string  value
         * @param integer length Inteiro não negativo
         * 
         * @return boolean
         */
        minLength: function(value, length) {
            if (typeof value !== 'string' || false === rule.type(length, 'number') || length < 0) {
                throw 'Um ou mais argumentos de minLength são inválidos.';
            }

            return (value.length < length) ? false : true;
        },

        /**
         * Verifica se o tamanho de uma string é igual ao valor definido.
         * 
         * Modo de usar: { "exactLength": 10 }
         * 
         * @param string  value
         * @param integer length Inteiro não negativo
         * 
         * @return boolean
         */
        exactLength: function(value, length) {
            if (typeof value !== 'string' || false === rule.type(length, 'number') || length < 0) {
                throw 'Um ou mais argumentos de exactLength são inválidos.';
            }

            return (value.length === length) ? true : false;
        },

        /**
         * Verifica se o valor recebido possui apenas caracteres alfabéticos.
         * 
         * Modo de usar: { "alpha": true }
         * 
         * @param string value
         */
        alpha: function(value, param) {
            return /^[a-zA-ZÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖßÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ]+$/.test(value);
        },

        /**
         * Verifica se o valor recebido contém apenas caracteres alfabéticos e 
         * numéricos.
         * 
         * Modo de usar: { "alphaNumeric": true }
         * 
         * @param string value
         */
        alphaNumeric: function(value, param) {
            return /^[a-zA-Z0-9ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖßÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ]+$/.test(value);
        },

        /**
         * Verifica se o valor recebido contém apenas caracteres alfabéticos, 
         * numéros e espaço.
         * 
         * Modo de usar: { "alphaNumericSpace": true }
         * 
         * @param string value
         */
        alphaNumericSpace: function(value, param) {
            return /^[a-zA-Z0-9ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖßÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ\s]+$/.test(value);
        },

        /**
         * Verifica se a data está no formato utilizado no Brasil: dd/mm/yyyy.
         * 
         * ATENÇÃO: esta função valida apenas o formato. Para validar a data, utilize
         * a função "date".
         * 
         * Modo de usar: { "checkDateFormatBR": true }
         * 
         * @param string value a data a ser checado o formato.
         * @param null    param
         */
        checkDateFormatBR: function(value, param) {
            return /^[0-9]{2}[\/][0-9]{2}[\/][0-9]{4}$/.test(value);
        },

        /**
         * Verifica se a data está no formato ISO 8601 (YYYY-MM-DD). Este é o formato
         * recomendado para ser utilizado em campos do tipo "date" no banco de dados
         * PostgreSQL.
         * 
         * ATENÇÃO: esta função valida apenas o formato. Para validar a data, utilize
         * a função "validate_date".
         * 
         * Modo de usar: { "checkDateFormatISO": true }
         * 
         * @param string value A data a ser checado o formato.
         * @param null   param
         */
        checkDateFormatISO: function(value, param) {
            return /^[0-9]{4}[\-][0-9]{2}[\-][0-9]{2}$/.test(value);
        },

        /**
         * Verifica se um número é maior ou igual ao valor mínimo definido.
         * 
         * @param number value número a ser verificado.
         * @param number limit valor mínimo a ser comparado.
         * 
         * @return booleam
         */
        minimum: function(value, limit) {
            if (!rule.type(value, 'number') || !rule.type(limit, 'number')) {
                throw 'Argumento de minimum é inválido.';
            }

            return (value >= limit) ? true : false;
        },

        /**
         * Verifica se um número é menor ou igual ao valor máximo definido.
         * 
         * @param number value número a ser verificado.
         * @param number limit valor máximo a ser compardo.
         * 
         * @return boolean
         */
        maximum: function(value, limit) {
            if (!rule.type(value, 'number') || !rule.type(limit, 'number')) {
                throw 'Argumento de maximum é inválido.';
            }

            return (value <= limit) ? true : false;
        },

        /**
         * Verifica se a data do argumento "value" é maior ou igual a 
         * "reference".
         * 
         * @param string value     Data a ser comparada
         * @param string reference Data de referência. Pode ser uma data ou o 
         * nome de uma proriedade da instância de dados que contém uma data.
         * 
         * @return boolean
         */
        minDate: function(value, reference) {
            return compareDate(value, {"option": 'minDate', "referenceDate": reference});
        },

        /**
         * Verifica se a data passada no argumento "value" é menor ou igual a 
         * "reference".
         * 
         * @param string value     Data a ser comparada
         * @param string reference Data de referência. Pode ser uma data ou o 
         * nome de uma proriedade da instância de dados que contém uma data.
         * 
         * @return boolean
         */
        maxDate: function(value, reference) {
            return compareDate(value,{"option": 'maxDate', "referenceDate": reference});
        },

        /**
         * Valida uma matriz de dados contra o um schema de regras.
         * 
         * @param array  value  Matriz com elementos a serem verificados.
         * @param object schema Objeto contendo regras a serem confrontadas com a matriz de dados "value".
         * 
         * @return boolean Retorna true se a validação for bem sucedida ou se a matriz estiver vazia.
         */
        items: function(value, schema) {
            if (!rule.type(value, 'array') || !schema || typeof schema !== 'object') {
                throw 'Um ou mais argumentos de items é inválido ou vazio.';
            }

            // Nome das propriedades de "schema".
            var schemaIndexes = Object.getOwnPropertyNames(schema),
            
            // Quantidade de propriedades.
            schemaLength = schemaIndexes.length;
            
            // Percorre todas as propriedade de "schema"
            for (var i = 0; i < schemaLength; i++) {
                switch (schemaIndexes[i]) {
                    case 'type':
                        for (var x = value.length - 1; x >= 0; x--) {
                            /**
                             * schema[schemaIndexes[i]] contém o tipo de dado a
                             * ser checado, por exemplo, "number", "string", etc.
                             */
                            if (false === rule.type(value[x], schema[schemaIndexes[i]])) {
                                return false;
                            }
                        }
                        break;
                }
            }

            return true;
        },

        /**
         * Verifica se a matriz de dados "items" é maior ou igual a "size".
         * 
         * @param array  items Instância de um array.
         * @param number size  Número inteiro não negativo.
         * 
         * @return boolean
         */
        minItems: function(items, size) {
            if (!rule.type(items, 'array') || size != parseInt(size) || size < 0) {
                throw 'Um ou mais argumentos de minItems é inválido ou vazio.';
            }

            return (items.length >= size) ? true : false;
        },

        /**
         * Verifica se uma matriz de dados possui itens únicos.
         * 
         * @param array   items  Instância de um array a ser validada.
         * @param booleam unique Se este argumento for "false" então a validação será considerada bem sucedida.
         * 
         * @return booleam
         */
        uniqueItems: function(items, unique) {
            if (!rule.type(items, 'array') || !rule.type(unique, 'boolean')) {
                throw 'Um ou mais argumentos de uniqueItems é inválido.';
            }

            if (unique === false) {
                return true;
            }

            var filteredItems = items.filter(function(value, index, self) {
                return self.indexOf(value) === index;
            });

            return (items.length === filteredItems.length) ? true : false;
        }
    };

    return {
        "init": function(schemas) {
            return init(schemas);
        },
        "run": function(dados, schema) {
            return run(dados, schema);
        },
        "getMessagesPlainText": function() {
            return getMessagesPlainText();
        },
        "getMessagesHTML": function() {
            return getMessagesHTML();
        },
        "version": function() {
            return version;
        }
    }

    /**
     * Carrega um o mais schemas para o objeto "schema".
     * 
     * @param array schemas Matriz simples contendo a URL do esquema a ser 
     * carregado ou o próprio objeto do esquema.
     * 
     * @return void 
     */
    function init(schemas) {
        if (!Array.isArray(schemas) || !schemas.hasOwnProperty(0)) {
            return;
        }

        for (var i = 0; i < schemas.length; i++) {
            if (schemas[i].hasOwnProperty('properties')) {
                if (!schemas[i].hasOwnProperty('id')) {
                    console.log('O esquema precisa ter um "id".');
                    continue;
                }
                
                schema[schemas[i].id] = schemas[i];
            } else {
                loadFileSchema(schemas[i]);
            }
        }
    }

    /**
     * Carrega um esquema através da URL passada por parâmetro.
     * 
     * @param string url
     * 
     * @return void 
     */
    function loadFileSchema(url) {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                var objResponse = JSON.parse(this.responseText);

                if (!objResponse.hasOwnProperty('id')) {
                    console.log('O esquema ' + url + ' precisa ter um "id".');
                    return;
                }''

                schema[objResponse.id] = objResponse;
            }
        };

        xmlhttp.open("GET", url, true);
        xmlhttp.send();
    }

    /**
     * @param object data   dados a serem processados.
     * @param object schema Definições de filtros e regras pelos quais os dados serão submetidos.
     * 
     * @return array|boolean se o processo de validação falhar em qualquer uma
     * das regras definidas, então o retorno será "false". Os dados filtrados 
     * serão retornados quando a validação for bem sucedida em 100% das regras,
     * ou, quando não houver regras definidas, mas apenas filtros.
     */
    function run(idSchema, data) {
        arrMensagens = [];

        try {
            if (!data || typeof data !== 'object') {
                throw 'O argumento 2 (data) não é um objeto.';
            }

            if (!idSchema || !schema.hasOwnProperty(idSchema)) {
                throw 'O esquema informado (' + idSchema + ') não existe ou não foi carregado.';
            }

            var dadosLength = Object.getOwnPropertyNames(data).length,
                restricoesLength = Object.getOwnPropertyNames(schema[idSchema].properties).length;

            if (dadosLength <= 0) {
                throw 'O argumento 1 (data) está vazio';
            }

            if (restricoesLength <= 0) {
                throw 'O argumento 2 (schema) está vazio';
            }
            
            dataObject = data;
            filteredDataObject = JSON.parse(JSON.stringify(data));
            schemaProperties = schema[idSchema].properties;

            // Verifica se foi definidos campos obrigatórios.
            if (Array.isArray(schema[idSchema].required) || schema[idSchema].required.hasOwnProperty(0)) {
                checkRequiredFields(schema[idSchema].required);
            }

            /**
             * "getOwnPropertyNames" Retorna um vetor com todas as propriedades 
             * (enumeráveis ou não) encontradas diretamente.
             * 
             * arrIndexRestricoes lista os índices do objeto "schema"
             * arrIndexRestricoes lista os índices do objeto "data"
             */
            var arrIndexRestricoes = Object.getOwnPropertyNames(schema[idSchema].properties);

            /**
             * É necessário recontar as propriedades porque a função 
             * "checkRequiredFields" pode exluir propriedades obrigatórias 
             * que estejam ausentes em "dataObject".
             */
            restricoesLength = Object.getOwnPropertyNames(schema[idSchema].properties).length;

            filtrar(arrIndexRestricoes, restricoesLength);
            validar(arrIndexRestricoes, restricoesLength);
            
            return (arrMensagens.length === 0) ? filteredDataObject : false;
        } catch (ex) {
            console.log(ex);
            return false;
        }
    }

    /**
     * Verifica se um camo obrigatório está presente em "dataObject".
     * 
     * @param array arrRequiredFields Lista de campos obrigatórios. 
     * 
     * @return void
     */
    function checkRequiredFields(arrRequiredFields) {
        // Adiciona o array de campos obrigatório à varialel global da classe.
        schemaRequiredFields = arrRequiredFields;
        var arrLength = schemaRequiredFields.length;

        for (var i = 0; i < arrLength; i++) {
            if (false === rule.required(schemaRequiredFields[i])) {
                setErrorMessage('required', '', schemaRequiredFields[i], '');

                /**
                 * Se o campo obrigatório não está presente em "dataObject",
                 * então suas propriedades serão removidas do objeto
                 * "schemaProperties" para o tal campo não seja processado nos
                 * método de filtros/regras. 
                 */
                delete schemaProperties[schemaRequiredFields[i]];
            }
        }
    }

    /**
     * Filtra todos os dados e hidrata o objeto "dataObject".
     * 
     * @param array   arrIndexRestricoes lista de nomes das restrições definidas.
     * @param integer restricoesLength   quantidade de restrições definidas.
     * 
     * @return void
     */
    function filtrar(arrIndexRestricoes, restricoesLength) {
        // Percore o objeto de dataObject.
        for (var i = 0; i < restricoesLength; i++) {
            if (!schemaProperties[arrIndexRestricoes[i]].filters) {
                continue;
            }

            var fieldName = arrIndexRestricoes[i];

            // Extrai o objeto que contem todas as definições de filtros do campo.
            var objFiltros = schemaProperties[fieldName].filters;
            
            // Contabiliza quantas propriedades (filtros) possui o objeto.
            var filtersLength = Object.getOwnPropertyNames(objFiltros).length;
            
            // Cria um array com o nome de cada filtro.
            var arrFiltersName = Object.getOwnPropertyNames(objFiltros);
            
            for (var f = 0; f < filtersLength; f++) {
                if (false === filter.hasOwnProperty(arrFiltersName[f])) {
                    throw 'A propriedade ' + arrFiltersName[f] + ' de "filters" é inválida.';
                }

                if (typeof filter[arrFiltersName[f]] === 'function') {
                    // Executa o filtro.
                    filteredDataObject[fieldName] = filter[arrFiltersName[f]](dataObject[fieldName]);
                }
            }
        }
    }

    /**
     * Valida todos dados que possuem regras definidas.
     * 
     * @param array   arrIndexRestricoes lista de nomes das restrições definidas.
     * @param integer restricoesLength   quantidade de restrições definidas.
     * 
     * @return void
     */
    function validar(arrIndexRestricoes, restricoesLength) {
        for (var i = 0; i < restricoesLength; i++) {
            if (!schemaProperties[arrIndexRestricoes[i]].rules) {
                continue;
            }

            var fieldName = arrIndexRestricoes[i],

            // Extrai o objeto que contem todas as regras do campo.
            objRules = schemaProperties[arrIndexRestricoes[i]].rules,

            // Contabiliza quantas propriedades (regras) possui o objeto.
            rulesLength = Object.getOwnPropertyNames(objRules).length,

            // Cria um array com o nome de cada regra.
            arrRulesName = Object.getOwnPropertyNames(objRules);

            for (var r = 0; r < rulesLength; r++) {
                var fnName = arrRulesName[r],
                    ruleDefinition = objRules[arrRulesName[r]];
3
                /**
                 * Esta condição será satisfeita quando a regra fazer
                 * referência a um campo inexistente.
                 */
                if (false === dataObject.hasOwnProperty(fieldName)) {
                    continue;
                }

                if (false === rule.hasOwnProperty(fnName)) {
                    throw 'A propriedade ' + fnName + ' de "rules" é inválida.';
                }

                if (typeof rule[fnName] === 'function') {
                    /**
                     * As funções de validação trabalham com um ou dois argumentos,
                     * sendo que o segundo argumento pode ser um valor literal ou
                     * uma referência para uma propriedade da instância de dados.
                     */
                    if (ruleDefinition.hasOwnProperty("prop") && dataObject.hasOwnProperty(ruleDefinition.prop)) {
                        ruleDefinition = dataObject[ruleDefinition.prop];
                    }

                    // Executa a validação.
                    if (false === rule[fnName](filteredDataObject[fieldName], ruleDefinition)) {
                        setErrorMessage(fnName, ruleDefinition, fieldName, dataObject[fieldName]);

                        /**
                         * Se o tipo de dados estiver errado então não é 
                         * necessário validar as demais propriedades do campo. 
                         */
                        if (fnName === 'type') {
                            break;
                        }
                    }
                }
            }
        }
    }

    /**
     * @param string rule           nome da regra
     * @param string ruleDefinition definição/argumentos da regra
     * @param string field          nome do campo testado
     * @param string fieldValue     valor testado
     */
    function setErrorMessage(rule, ruleDefinition, field, fieldValue) {
        /**
         * Se um apelido foi definido, então ele será exibido no lugar do nome 
         * do camo propriamente dito.
         */
        if (schemaProperties.hasOwnProperty(field) && schemaProperties[field].alias) {
            field = schemaProperties[field].alias;
        }

        switch (rule) {
            case 'required':
                arrMensagens.push('O campo <b>' + field + '</b> é obrigatório.');
                break;
            case 'type':
                arrMensagens.push('O tipo de dado do campo <b>' + field + '</b> é inválido.');
                break;
            case 'email':
                arrMensagens.push('O e-mail <b>' + fieldValue + '</b> é inválido.');
                break;
            case 'cpf':
                arrMensagens.push('O CPF <b>' + fieldValue + '</b> é inválido.');
                break;
            case 'date':
                arrMensagens.push('A data <b>' + fieldValue + '</b> do campo <b>' + field + '</b> é inválida.');
                break;
            case 'enum':
                arrMensagens.push(
                    'O campo <b>' + field + 
                    '</b> deve conter um dos seguintes valores: ' + ruleDefinition.join(', ') + '.');
                break;
            case 'maxLength':
                arrMensagens.push(
                    'O campo <b>' + field + 
                    '</b> não pode ultrapassar ' + ruleDefinition + ' caracter(res).');
                break;
            case 'minLength':
                arrMensagens.push(
                    'O campo <b>' + field + 
                    '</b> deve conter no mínimo ' + ruleDefinition + ' caracter(res).');
                break;
            case 'exactLength':
                arrMensagens.push(
                    'O campo <b>' + field + 
                    '</b> deve conter o número exato de ' + ruleDefinition + ' caracter(res).');
                break;
            case 'alpha':
                arrMensagens.push('O campo <b>' + field + '</b> deve conter apenas caracteres alfabéticos.');
                break;
            case 'alphaNumeric':
                arrMensagens.push('O campo <b>' + field + '</b> deve conter apenas caracteres alfabéticos e números.');
                break;
            case 'alphaNumericSpace':
                arrMensagens.push(
                    'O campo <b>' + field + 
                    '</b> deve conter apenas caracteres alfabéticos, números e espaços.');
                break;
            case 'checkDateFormatBR':
                arrMensagens.push(
                    'O formato da data <b>' + fieldValue + 
                    '</b> é inválido. O formato aceitável é DD/MM/YYYY (DIA/MÊS/ANO).');
                break;
            case 'checkDateFormatISO':
                arrMensagens.push(
                    'O formato da data <b>' + fieldValue + 
                    '</b> é inválido. O formato aceitável é YYYY-MM-DD (ANO-MÊS-DIA).');
                break;
            case 'minimum':
                arrMensagens.push('O campo <b>' + field + '</b> precisa ser maior ou igual a ' + ruleDefinition + '.');
                break;
            case 'maximum':
                arrMensagens.push('O campo <b>' + field + '</b> precisa ser menor ou igual a ' + ruleDefinition + '.');
                break;
            case 'minDate':
                arrMensagens.push(
                    'A data <b>' + fieldValue + '</b> do campo <i>"' + field + '"</i>' +
                    ' precisa ser maior ou igual a <b>' + ruleDefinition + '</b>.');
                break;
            case 'maxDate':
                arrMensagens.push(
                    'A data <b>' + fieldValue + '</b> do campo <i>"' + field + '"</i>' +
                    ' precisa ser menor ou igual a <b>' + ruleDefinition + '</b>.');
                break;
            case 'minItems':
                arrMensagens.push(
                    'Infome pelo menos ' + ruleDefinition + ' opção(ões) de <b>' + field + '</b>.');
                break;
            case 'uniqueItems':
                arrMensagens.push(
                    '<b>' + field + '</b> não pode conter valores repetidos.');
                break;
            default:
                arrMensagens.push('O campo <b>' + field + '</b> é inválido.');
                break;
        }
    }

    /**
     * Retorna as mensagens de erro formatadas com tags HTML.
     * 
     * @return string
     */
    function getMessagesHTML() {
        // Implode o array e adiciona quebra de linha no final de cada mensagem.
        return arrMensagens.join('<br>');
    }

    /**
     * Converte o array de mensagens de erro numa string de texto sem formatação.
     * Essa string é ideal para ser exibida através da função "alert".
     * 
     * @return string
     */
    function getMessagesPlainText() {
        /**
         * Implode o array, adiciona quebra de linha ao finalde cada menagem e 
         * remove as tags html da string.
         */
        return arrMensagens.join('\n').replace(/<[^>]*>/g, '');
    }

    /**
     * Verifica se um data é maior ou menor que uma determinada data de referência.
     * 
     * @param date   value data a ser comparada
     * @param object param objeto contendo a data de referência e a opção de 
     * comparação (min_date|max_date). 
     *     param = {"option": 'min_date', "referenceDate": '01/01/2018'}
     */
    function compareDate(value, param) {
        if ((!value && !param.referenceDate) || (
                (typeof value === 'string' && !value.trim()) && 
                (typeof param.referenceDate === 'string' && !param.referenceDate.trim())
            )
        ) {
            return true;
        }

        var arrData = filter.dateToArray(value),
            arrDataReferecencia = filter.dateToArray(param.referenceDate);

        if (arrData === false || arrDataReferecencia === false) {
            return false;
        }

        var data = new Date(
            parseInt(arrData[2]),
            parseInt(arrData[1]) -1,
            parseInt(arrData[0])
        ),
        
        dataReferencia = new Date(
            parseInt(arrDataReferecencia[2]),
            parseInt(arrDataReferecencia[1]) -1,
            parseInt(arrDataReferecencia[0])
        );
        
        if (param.option === 'minDate') {
            return (data >= dataReferencia) ? true : false;    
        }

        return (data <= dataReferencia) ? true : false;
    }
})();
